#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ommqttclass.h>


#include "databaseclass.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void insert();
    void lcdValueInsert(double Feuchtiggkeit, double Temperatur, double durchFeuchtiggkeit, double durchTemperatur);

private slots:


private:
    Ui::MainWindow *ui;


    DatabaseClass db;
    omMQTTClass omMQT;
};
#endif // MAINWINDOW_H
