#include "ommqttclass.h"


omMQTTClass::omMQTTClass()
{

}
//MQTT Broker adresse: ssl://dashboard-stage.opticloud.io:8883

//Die Funktion um mich mit dem MQTT Broker zu verbinden
QString omMQTTClass::mqttConnect()
{    
    //qDebug() << "mqttConnect aufgerufen" << Qt::endl;
    m_client = new QMqttClient(this);

    m_client->setHostname("dashboard-stage.opticloud.io");
    m_client->setClientId("smartMINI-S 21004700172");
    m_client->setUsername("21004700172");
    m_client->setPassword("21004700172");
    m_client->setPort(8883);

    connect(m_client, &QMqttClient::messageReceived, this, &omMQTTClass::handleMessage);
    mqttSubscribe();

    connect(m_client, &QMqttClient::errorChanged, this, [](QMqttClient::ClientError error){
        qDebug() << "Error = " << error;});

    connect(m_client, &QMqttClient::disconnected, this, [](){qDebug() << "Disconnected";});

    QSslConfiguration sslconfig;

    //m_client->connectToHostEncrypted(sslconfig); //Notwendig wenn man eine verschlüsselte verbindung benutzt
    m_client->connectToHostEncrypted(sslconfig); //falls eine verschlüsselte Verbindung benutzt wird muss man connectToHostEncrypted() benutzten

    return _Content;
}

void omMQTTClass::mqttSubscribe()
{
    connect(m_client, &QMqttClient::connected, this, [this](){

        qDebug() << "connected";

        m_client->subscribe(TELEMETRY_TOPIC_JSON, 0);
        m_client->subscribe(ATTRIBUTE_RESPONSE_ON_REQUEST_TOPIC_JSON, 0);
        m_client->subscribe(ATTRIBUTE_RESPONSE_ON_CHANGE_TOPIC_JSON, 0);

        QJsonObject joParameterRequest;
        joParameterRequest.insert("sharedKeys","qtTest");
        QJsonDocument jdParameterRequest(joParameterRequest);
        m_client->publish(ATTRIBUTE_REQUEST_TOPIC_JSON, jdParameterRequest.toJson(QJsonDocument::Compact));
        });
}

//payload.append("{\"sharedKeys\" : \"" + m_channelName.toLatin1() + "\"}");

//Funktion um die ein Signal zu bekommen das die daten vom mqtt server emofangen wurden.
void omMQTTClass::handleMessage(const QByteArray &message, const QMqttTopicName &topic)
{
    //qDebug() << "handleMessage aufgerufen";
    qDebug() << topic;
    qDebug() << message;
    emit messageReceived(message);
}
