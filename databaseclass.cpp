#include "databaseclass.h"

DatabaseClass::DatabaseClass()
{

}

//Funktion zur verbindung mit der Datenbank
void DatabaseClass::databaseConnect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("omRaumklimaAnzeige.de");
    db.open();

    if(!db.open())
    {
        qDebug() << db.lastError() << Qt::endl;
    }
}

//Funkton um die bekommenen daten vom MQTT Broker in die DB zu importieren.
void DatabaseClass::insertNormalValues(float temperatur, int zeit, QString typ)
{
    QSqlQuery insertQuery;

    insertQuery.prepare("INSERT INTO Messdaten(Temperatur_Grad, Timestamp, Typ) VALUES(:temperatur, :zeit, :typ)");
    insertQuery.bindValue(":temperatur", temperatur);
    insertQuery.bindValue(":zeit", zeit);
    insertQuery.bindValue(":typ",typ);
    insertQuery.exec();
}

//Funktion um die die Importiereten werte aus der Datenbank zu holen
//und zuzeit auch den Druchschnittswet zu berechnen
float DatabaseClass::takeNormalValues(int durchTyp)
{
    QSqlQuery takeQuery;

    float gesamtTemp = takeQuery.exec("SELECT SUM(Temeratur_Grad) FROM Messdaten");

    switch (durchTyp) {
    case 1:
        //Tag
        durchschnittsTemp = gesamtTemp / 86400;
        break;
    case 2:
        //Woche
        durchschnittsTemp = gesamtTemp / 604800;
        break;
    case 3:
        //Monat
        durchschnittsTemp = gesamtTemp / 2592000;
        break;
        default:
        qDebug() << "Fehler: Kein gültiger Typ angegeben" << Qt::endl;
        break;
    }

    return durchschnittsTemp;
}

//Durchschnittliche werte in die db importieren
void DatabaseClass::insertDurchValues(int durchtyp, float durchschnittsTemp, int messdatenTyp)
{
    QSqlQuery insertQuery;

    insertQuery.prepare("INSERT INTO Durchschnittswerte(Durchschnittswert, timestamp, DW_Typ, DW_Messdaten_Typ)"
                        "VALUES(:duchschnittsTemp, :timestamp, :DW_typ, DW_Messdaten_Typ)");

    insertQuery.bindValue(":durchschnittsTemp", durchschnittsTemp);
    insertQuery.bindValue(":timestamp", 5); //Die 5 ist nur ien platzhalter
    insertQuery.bindValue(":DW_typ", durchtyp);
    insertQuery.bindValue(":DW_Messdaten_Typ", messdatenTyp);
    insertQuery.exec();
}

//Muss noch überarbeitet werden
float DatabaseClass::takeDurchValues(int durchschnittsArt, int durchMessdatenTyp)
{
    QSqlQuery takeQuery;

    durchschnittsTemp = takeQuery.exec("SELECT Durchschnittswert, timestamp FROM Durchschnittswerte "
                                       "WHERE :DW_Typ = DW_typ AND DW_Messdaten_Typ = :messdatenTyp");
    while(takeQuery.next())
    {
        takeQuery.bindValue(":DW_Typ", durchschnittsArt);
        takeQuery.bindValue(":messdatenTyp", durchMessdatenTyp);
    }
    return durchschnittsTemp;
}
