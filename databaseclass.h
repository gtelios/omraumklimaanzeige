#ifndef DATABASECLASS_H
#define DATABASECLASS_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>

class DatabaseClass
{
public:
    DatabaseClass();


    void databaseConnect();
    void insertNormalValues(float, int, QString);
    float takeNormalValues(int);
    void insertDurchValues(int, float, int);
    float takeDurchValues(int, int);

private:

    float durchschnittsTemp;

};

#endif // DATABASECLASS_H
