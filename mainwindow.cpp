#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db.databaseConnect();
    insert();
    lcdValueInsert(11, 15, 8, 13);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::lcdValueInsert(double Feuchtiggkeit, double Temperatur, double durchFeuchtiggkeit, double durchTemperatur)
{
    ui->lcdNumberFeuchtichkeits->display(Feuchtiggkeit);
    ui->lcdNumberTemperatur->display(Temperatur);
    ui->lcdNumberFeuchtichkeits_Durchschnitt->display(durchFeuchtiggkeit);
    ui->lcdNumberTemperatur_Durchschnitt->display(durchTemperatur);
}

void MainWindow::insert()
{
    //qDebug() << omMQT.mqttConnect() << Qt::endl;
    ui->testBrowser->insertPlainText(omMQT.mqttConnect());
    //omMQT.handleMessage();
}
