#ifndef OMMQTTCLASS_H
#define OMMQTTCLASS_H


#include <QtMqtt/QtMqtt>
#include <QMqttClient>
#include <QMqttMessage>
#include <QMqttSubscription>
#include <QDateTime>



class omMQTTClass : public QMqttClient
{
public:
    omMQTTClass();
    QString mqttConnect();
    QMqttSubscription* subscribe(const QString &topic);
    void updateLogStateChange();
    void handleMessage(const QByteArray &message, const QMqttTopicName &topic);
    void mqttSubscribe();

private:

    const QString ATTRIBUTE_RESPONSE_ON_REQUEST_TOPIC_JSON = "v1/devices/me/attributes/response/+" ;
    const QString ATTRIBUTE_RESPONSE_ON_CHANGE_TOPIC_JSON = "v1/devices/me/attributes";
    const QString ATTRIBUTE_REQUEST_TOPIC_JSON = "v1/devices/me/attributes/request/1";
    const QString TELEMETRY_TOPIC_JSON = "v1/devices/me/telemetry";

    QMqttMessage qMesseage;
    QMqttClient *m_client;
    QString _Content;
};
#endif // OMMQTTCLASS_H
